# SC19 Tutorial

## Agenda

* [Intro - Unified Cyber Infrastructure - Slides](slides/1_sc19_k8s_intro.pptx)

* [Kubernetes Architecture - Slides](slides/2_sc19_kubernetes_arch.pptx)

* [Basic Kubernetes - Hands On (part 1)](2a_Basic hands on.md) [part 2](2a_Basic hands on Part two.md)

* [Kubernetes Scheduling - Slides](slides/3_sc19_kubernetes_sched.pptx)

* [Scheduling hands-on](3a_Scheduling hands on.md)

* [Persistent Storage - Slides](slides/4_sc19_kubernetes_rook.pptx)

* [Storage hands-on](4a_Storage hands on.md)

* [Monitoring - Slides](slides/5_sc19_kubernetes_monitoring.pptx)

* [Monitoring hands-on](5a_Monitoring hands on.md)

* [How to install Kubernetes - Slides](slides/6_sc19_k8s_install.pptx)

* [Kubernetes Federation options - Slides](slides/7_sc19_kubernetes_federation.pptx)

* [PRP and Nautilus - Slides](slides/8_sc19_prp_nautilus.pptx)

* [OSG use of PRP - Slides](slides/9_sc19_k8s_osg.pptx)
