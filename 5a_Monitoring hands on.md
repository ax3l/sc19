# SC 2019

# Kubernetes monitoring

There are multiple monitoring tools available for kubernetes. The one that was specially designed for kubernetes and supported by Cloud Native Association is called prometheus.

In general it's a time-series database scraping known daemons for information and storing those.

Go to <https://prometheus.nautilus.optiputer.net/targets> and look at different types of targets it's pointed at.

The common tool to visualize these metrics is called grafana. Our grafana is running at [https://grafana.nautilus.optiputer.net](https://grafana.nautilus.optiputer.net/?orgId=1)

Login with password provided and browse the cluster status.

It's possible to extend the current set of dashboards and create our own. Let's first see what's available. In <https://prometheus.nautilus.optiputer.net/graph> start typing "pod" and choose some metric to look at, for example `kube_pod_owner`.

```
kube_pod_owner{instance="10.244.1.6:8443",job="kube-state-metrics",namespace="amtseng",owner_is_controller="true",owner_kind="Job",owner_name="e2f6-encode",pod="e2f6-encode-rlqlk"}	1
kube_pod_owner{instance="10.244.1.6:8443",job="kube-state-metrics",namespace="amtseng",owner_is_controller="true",owner_kind="Job",owner_name="e2f6-encode",pod="e2f6-encode-v2lhg"}	1
kube_pod_owner{instance="10.244.1.6:8443",job="kube-state-metrics",namespace="amtseng",owner_is_controller="true",owner_kind="Job",owner_name="spi1-encode",pod="spi1-encode-nrqld"}	1
kube_pod_owner{instance="10.244.1.6:8443",job="kube-state-metrics",namespace="amtseng",owner_is_controller="true",owner_kind="Job",owner_name="spi1-encode-priors",pod="spi1-encode-priors-bc8l2"}	1
kube_pod_owner{instance="10.244.1.6:8443",job="kube-state-metrics",namespace="amtseng",owner_is_controller="true",owner_kind="Job",owner_name="tead4-encode-priors",pod="tead4-encode-priors-nk7tx"}	1
kube_pod_owner{instance="10.244.1.6:8443",job="kube-state-metrics",namespace="amtseng",owner_is_controller="true",owner_kind="Job",owner_name="tead4-encode-random-priors",pod="tead4-encode-random-priors-dzwnk"}
```

You see the metric values along with their labels in curly braces. Labels are what makes prometheus and it's query language (PromQL) so powerful - you can join different metrics based on labels, like in SQL, but for time series.

Now let's get some metrics for the pods we were running. Go to the Graph section of the page (or click <https://prometheus.nautilus.optiputer.net/graph>) and make the query, replacing the `your_pod_name` with actual pod name that is still running in the namespace or was running before

```
node_namespace_pod_container:container_cpu_usage_seconds_total:sum_rate{namespace="sc19-demo",pod="<your_pod_name>"}
```

On the graph you'll see the plot for your pod's CPU usage. Prometheus queried the docker exporter endpoint to get this data and stored in it's time series database.

The prometheus page provides a nice way to debug the queries, but it's lacking a lot of needed functionality to really dive into the data. Grafana is a tool that can connect to multiple time series databases, including prometheus, and show the data from it.

Login to grafana at https://grafana.nautilus.optiputer.net, and create a dashboard by clicking the + button and choosing the "Choose visualization".

Once everyone is there, I will show in real time how to create things, and you can experiment with it yourself. Don't forget to choose the Prometheus data source.

You can enter the query we used before as a starter.

# Metric joins

As we discussed before, joins are a powerful way to display the data. Let's combine some metrics to see how it works.

This is how you can get the average IO Wait metric for all pods running on a specific node in a cluster:

```
avg(
  irate(
    nodecpusecondstotal{mode="iowait"}[5m]
  ) * 
  on(namespace, pod) groupleft(node)
    nodenamespacepod:kubepodinfo:{node="$node"}
) * 100
```

# Using grafana

To plot the data, we can use grafana plotting tool.

Login to <https://grafana.nautilus.optiputer.net>, `sc19demo:sc19demo`

Create a new dashboard, choose `prometheus` as your data source and enter the metric we looked at in prometheus. It will display the CPU usage of your pod.

Add some more metrics you'd like to see there. You can also explore the existing dashboards and see how those are built.