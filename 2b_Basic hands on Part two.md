# SC19 Tutorial

Basic Kubernetes\
Hands on session\
Part 2

## Horizontal scaling

Orchestration is often used to spread the load over multiple nodes.

In this exercise, we will launch multiple Web servers.

To make distinguishing the two servers easier, we will force the nodename into their homepages. Using stock images, we achieve this by using an init container.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
All the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

###### http2.yaml:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: http-username
  labels:
    k8s-app: http-username
spec:
  replicas: 2
  selector:
    matchLabels:
      k8s-app: http-username
  template:
    metadata: 
      labels:
        k8s-app: http-username
    spec:
      initContainers:
      - name: myinit
        image: busybox
        command: ["sh", "-c", "echo '<html><body><h1>I am ' `hostname` '</h1></body></html>' > /usr/local/apache2/htdocs/index.html"]
        volumeMounts:
        - name: dataroot
          mountPath: /usr/local/apache2/htdocs
      containers:
      - name: mypod
        image: httpd:alpine
        resources:
           limits:
             memory: 1.5Gi
             cpu: 1
           requests:
             memory: 0.5Gi
             cpu: 0.1
        volumeMounts:
        - name: dataroot
          mountPath: /usr/local/apache2/htdocs
      volumes:
      - name: dataroot
        emptyDir: {}
```

BTW: Feel free to change the number of replicas (within reason) and the text it is shown in home page of each server, if so desired.

Launch the deployment:

```
kubectl create -f http2.yaml
```

Also launch the pod1 from basic hand on excercise.

Check the pods you have, alongside the IPs they were assigned to:

```
kubectl get pods -o wide
```

Log into pod1

```
kubectl exec -it pod-username -- /bin/bash
```

Now try to pull the home pages from the two Web servers; use the IPs you obtained above:\
curl http://*IPofPod*

You should get a different answer from the two.

## Load balancing

Having to manually switch between the two Pods is obviously tedious. What we really want is to have a single logical address that will automatically load-balance between them.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
All the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

###### svc2.yaml:

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    k8s-app: svc-username
  name: svc-username
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    k8s-app: http-username
  type: ClusterIP
```

Let’s now start the service:

```
kubectl create -f svc2.yaml
```

Look up your service, and write down the IP it is reporting under:

```
kubectl get services
```

Log into pod1

```
kubectl exec -it pod-username -- /bin/bash
```

Now try to pull the home page from the service IP:\
curl http://*IPofService*

Try it a few times… Which Web server is serving you?

Note that you can also use the local DNS name for this (from pod1)

curl [http://svc-username.sc19-demo.svc.cluster.local](http://svc-username.gpn19-demo.svc.cluster.local)

## Exposing public services

Sometimes you have the opposite problem; you want to export resources of a single node to the public internet.

The above Web services only serve traffic on the private IP network LAN. If you try curl from your laptops, you will never reach those Pods!

What we need, is set up an Ingress instance for our service.

###### ingress.yaml:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: traefik
  name: ingress-username
spec:
  rules:
  - host: username.nautilus.optiputer.net
    http:
      paths:
      - backend:
          serviceName: svc-username
          servicePort: 80
        path: /
```

Launch the new ingress

```
kubectl create -f ingress.yaml
```

You should now be able to fetch the Web pages from your browser by opening <https://username.nautilus.optiputer.net>. Note that SSL termination is already provided for you.

You can now delete the deployment:

```
kubectl delete -f host1.yaml
```

## The end

Please make sure you did not leave any running pods, deployments, ingresses or services behind.
